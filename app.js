
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , socket = require('socket.io')
  , uuid = require('node-uuid')
  , path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(require('stylus').middleware(__dirname + '/public'));
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}


var server = require('http').createServer(app)
server.listen(3000,function(){
  console.log("el chat rockea!")
});
var io = require('socket.io').listen(server);
// io.set('transports', ['xhr-polling'])
var mensajes=[];
var usuarios=[];
var baneds=[];
var chat_curso1=io.of('/chat-curso1')
.on('connection', function (socket) {	
	socket.on('minombrees',function(data){
		var tokenId=uuid.v1();
		socket.name=data.nombre;
		socket.uuid=tokenId;
		socket.estado=1;//uno habilitado 0 baneado
		usuarios.push({
			 uuid:tokenId
			,name:socket.name
			,estado:1
		})
		socket.broadcast.emit('meconecte',socket.name);
	})
	socket.emit('conectado', 
		{ mensajes: mensajes}
	);
	socket.on('nuevoMensaje', function (data) {
		// var l=baneds.length;
		// for( var i=0;i<l;i++){
		// 	if(socket.uuid == baneds[i]){
		// 		socket.emit('baned','Usted ha sido baneado por el administrador')				
		// 	}else{
				mensajes.push({	msg:data.msg,name:socket.name,uuid:socket.uuid});
				socket.broadcast.emit('mensajeRecibido',{msg:data.msg,name:socket.name,uuid:socket.uuid})
		// 	}
		// }
	});
	socket.on('disconnect', function () { 
	socket.broadcast.emit('desconectado',{msg:'se ha desconectado '+socket.name}) 
	}); 

	socket.on('banear',function(data){
		baneds.push(data.uuid);		
	})
	socket.on('desbanear',function(){data})
		var l=baneds.length;
		for(var i=0;i<l;i++){
			if(baneds[i]==data.uuid){
				baneds[i]="";
			}
		}
});

app.get('/', routes.index);
app.get('/sockets',function(req,res){	
	res.render('camu',{users:usuarios,title:'WSmart | administracion'});
})

