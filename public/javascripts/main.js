
$(document).on("ready",init);
window.m;
window.storage;
window.yo;
var socket = io.connect('http://localhost:3000/chat-curso1');
function init(){
	
	$("form[name='chat-msg']").on("submit",function(e){		
		e.preventDefault();
		var t=$("#caja");
		enviarMsg(t.val());
		$("#mensajes").before(
			"<div class='msg'><i>"+window.yo+"</i> dijo : "+t.val()+"</div></br>"
		)
		t.val('')

	})

	$("#login-b").on("click",function(e){
		e.preventDefault();
		socket.emit('minombrees',{nombre:$("#login-t").val()});
		window.yo=$("#login-t").val();
		$("#login-div").fadeOut();
		$("form fieldset").delay(500).fadeIn();
		window.storage['nick']=$("#login-t").val()
		$("#mensajes").delay(500).fadeIn();
	})
	// v();

	$(".b").on("click",function(e){
		socket.emit('banear',{uuid:$(this).val()})
		$(this).removeClass('btn-danger')
		$(this).addClass('btn-success')
		$(this).text('Desbanear');
	})
	$("button").delegate('.btn-success','click',function(){
		socket.emit('desbanear',{uuid:$(this).val()})
		$(this).removeClass('btn-success')
		$(this).addClass('btn-danger')
		$(this).text('Banear');		
	})
}
(function storage(){	
	try {
		if (localStorage.getItem) {
			window.storage = localStorage;
		}
	} catch(e) {
		window.storage = {};
	}
})()
// validamos si el usuario ya se registro
function v(){
	var s=typeof(window.storage['nick'])
	if(s  == "undefined"){

	}else{
		$("#login-div").fadeOut();
		$("form fieldset").delay(500).fadeIn();
	}
}

// esta funcion envia los mensajes al servidor
function enviarMsg(mensaje){	
	socket.emit('nuevoMensaje',{msg:mensaje})
}

// funcion automatica para cargar los mensajes del clat
(function socketIO(){	
	socket.on('meconecte',function (data){
		$("#mensajes").before(
				"<div class='msg'><i> Se ha conectado: "+data+"</i></div></br>"
		)
	})
	socket.on('conectado', function (data) {
		window.m=data;
		console.log(data);
		var l=window.m.mensajes.length;
		for( var i=0;i<l;i++){
			console.log(window.m.mensajes[i].uuid)
			$("#mensajes").before(
				"<div class='msg'><i>"+window.m.mensajes[i].name+"</i> dijo : "+window.m.mensajes[i].msg+"</div></br>"
			)
		}
	});
	socket.on('mensajeRecibido',function(data){
		console.log(data.uuid)
		$("#mensajes").before(
			"<div class='msg'><i>"+data.name+"</i> dijo : "+data.msg+"</div></br>"
		)
	}) 
	socket.on('desconectado',function(data){
		$("#mensajes").before(
		"<div class='off'><i> "+data.msg+" </i></div></br>")
	})
	socket.on('baned',function(data){
		$("#mensajes").before(
			"<div class='msg'><i>"+data+"</i></div></br>"
		)
	})
})()